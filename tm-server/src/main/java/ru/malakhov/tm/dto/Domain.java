package ru.malakhov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Domain implements Serializable {

    public static final long serialVersionUID = 1;

    @NotNull
    private List<ProjectDto> projects = new ArrayList<>();

    @NotNull
    private List<TaskDto> tasks = new ArrayList<>();

    @NotNull
    private List<UserDto> users = new ArrayList<>();

    @NotNull
    private List<SessionDto> sessions = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Domain domain = (Domain) o;
        return projects.equals(domain.projects)
                && tasks.equals(domain.tasks)
                && users.equals(domain.users)
                && sessions.equals(domain.sessions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(projects, tasks, users, sessions);
    }

    public @NotNull List<ProjectDto> getProjects() {
        return projects;
    }

    public void setProjects(@NotNull List<ProjectDto> projects) {
        this.projects = projects;
    }

    public @NotNull List<TaskDto> getTasks() {
        return tasks;
    }

    public void setTasks(@NotNull List<TaskDto> tasks) {
        this.tasks = tasks;
    }

    public @NotNull List<UserDto> getUsers() {
        return users;
    }

    public void setUsers(@NotNull List<UserDto> users) {
        this.users = users;
    }

    public @NotNull List<SessionDto> getSessions() {
        return sessions;
    }

    public void setSessions(@NotNull List<SessionDto> sessions) {
        this.sessions = sessions;
    }

}
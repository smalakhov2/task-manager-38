package ru.malakhov.tm.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.dto.TaskDto;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.empty.EmptyNameException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;
import ru.malakhov.tm.exception.system.IndexIncorrectException;
import ru.malakhov.tm.repository.dto.TaskDtoRepository;
import ru.malakhov.tm.repository.entity.TaskRepository;

import java.util.ArrayList;
import java.util.List;

@Setter
@Service
public final class TaskService extends AbstractService<TaskDto, TaskDtoRepository> implements ITaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    public TaskService(@NotNull final TaskDtoRepository taskDtoRepository) {
        super(taskDtoRepository);
    }

    @Override
    @Transactional
    public void create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final TaskDto task = new TaskDto(name, "", userId);
        save(task);
    }

    @Override
    @Transactional
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final TaskDto task = new TaskDto(name, description, userId);
        save(task);
    }

    @NotNull
    @Override
    public List<Task> findAllEntity() {
        return taskRepository.findAll();
    }

    @NotNull
    public List<TaskDto> findAllDtoByUserId(
            @Nullable final String userId
    ) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final List<TaskDto> tasks = new ArrayList<>();
        dtoRepository.findByUserIdOrderByName(userId).forEach(tasks::add);
        return tasks;
    }

    @NotNull
    public List<Task> findAllEntityByUserId(
            @Nullable final String userId
    ) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final List<Task> tasks = new ArrayList<>();
        taskRepository.findByUserIdOrderByName(userId).forEach(tasks::add);
        return tasks;
    }

    @Nullable
    @Override
    public Task findOneEntityById(
            @Nullable final String id
    ) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public TaskDto findOneDtoByIdAndUserId(
            @Nullable final String id,
            @Nullable final String userId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return dtoRepository.findByIdAndUserId(id, userId).orElse(null);
    }

    @Nullable
    @Override
    public Task findOneEntityByIdAndUserId(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findByIdAndUserId(id, userId).orElse(null);
    }

    @Nullable
    @Override
    public TaskDto findOneDtoByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final int counter = (int) dtoRepository.count();
        if (index == null || index < 0 || index >= counter) throw new IndexIncorrectException();
        @NotNull final List<TaskDto> projects = new ArrayList<>();
        dtoRepository.findByUserIdOrderByName(userId).forEach(projects::add);
        return projects.get(index);
    }

    @Nullable
    @Override
    public Task findOneEntityByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final int counter = (int) dtoRepository.count();
        if (index == null || index < 0 || index >= counter) throw new IndexIncorrectException();
        @NotNull final List<Task> projects = new ArrayList<>();
        taskRepository.findByUserIdOrderByName(userId).forEach(projects::add);
        return projects.get(index);
    }

    @Nullable
    @Override
    public TaskDto findOneDtoByName(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return dtoRepository.findByUserIdAndName(userId, name).orElse(null);
    }

    @Nullable
    @Override
    public Task findOneEntityByName(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByUserIdAndName(userId, name).orElse(null);
    }

    @Override
    @Transactional
    public void removeAll() {
        taskRepository.deleteAll();
    }

    @Override
    @Transactional
    public void removeAllByUserId(@Nullable final String userId) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        taskRepository.deleteByUserId(userId);
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeOneByIdAndUserId(
            @Nullable final String id,
            @Nullable final String userId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.deleteByIdAndUserId(id, userId);
    }

    @Override
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final int counter = (int) dtoRepository.count();
        if (index == null || index < 0 || index >= counter) throw new IndexIncorrectException();
        @Nullable final Task project = findOneEntityByIndex(userId, index);
        if (project == null) return;
        taskRepository.deleteById(project.getId());
    }

    @Override
    @Transactional
    public void removeOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        taskRepository.deleteByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void updateTaskById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @Nullable final TaskDto task = findOneDtoByIdAndUserId(id, userId);
        if (task == null) return;
        task.setName(name);
        task.setDescription(description);
        save(task);
    }

    @Override
    @Transactional
    public void updateTaskByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @Nullable final TaskDto task = findOneDtoByIndex(userId, index);
        if (task == null) return;
        task.setName(name);
        task.setDescription(description);
        save(task);
    }

}
package ru.malakhov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.malakhov.tm.api.endpoint.IUserEndpoint;
import ru.malakhov.tm.api.service.ISessionService;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.dto.response.Fail;
import ru.malakhov.tm.dto.response.Result;
import ru.malakhov.tm.dto.response.Success;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.user.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
@NoArgsConstructor
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Override
    @WebMethod
    public Result updateUser(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "email") @Nullable final String email,
            @WebParam(name = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName") @Nullable final String lastName,
            @WebParam(name = "middleName") @Nullable final String middleName
    ) {
        try {
            sessionService.validate(session);
            userService.updateUserInfo(
                    session.getUserId(),
                    email,
                    firstName,
                    lastName,
                    middleName
            );
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result updateUserPassword(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "newPassword") @Nullable final String newPassword
    ) throws AbstractException {
        try {
            sessionService.validate(session);
            userService.updatePassword(
                    session.getUserId(),
                    password,
                    newPassword
            );
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result registryUser(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "email") @Nullable final String email
    ) throws AbstractException {
        try {
            userService.create(
                    login,
                    password,
                    email
            );
            return new Success();
        } catch (final AccessDeniedException e) {
            return new Fail(e);
        }
    }

}
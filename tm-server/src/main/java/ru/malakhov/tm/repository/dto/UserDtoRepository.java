package ru.malakhov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.dto.UserDto;

import java.util.Optional;

@Repository
public interface UserDtoRepository extends AbstractDtoRepository<UserDto> {

    @NotNull
    Optional<UserDto> findByLogin(String login);

}
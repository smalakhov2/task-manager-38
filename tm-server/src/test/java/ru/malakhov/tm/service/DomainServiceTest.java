package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.AbstractDataTest;
import ru.malakhov.tm.api.service.*;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.dto.*;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.EmptyDomainException;
import ru.malakhov.tm.exception.empty.EmptyIdException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class DomainServiceTest extends AbstractDataTest {

    @NotNull
    private final IDomainService domainService = context.getBean(DomainService.class);

    @NotNull
    private final ISessionService sessionService = context.getBean(SessionService.class);

    @NotNull
    private final IUserService userService = context.getBean(UserService.class);

    @NotNull
    private final IProjectService projectService = context.getBean(ProjectService.class);

    @NotNull
    private final ITaskService taskService = context.getBean(TaskService.class);

    @NotNull
    private final List<UserDto> allUserInSystem = new ArrayList<>(Arrays.asList(userDto, adminDto));

    @NotNull
    private final ProjectDto projectOne = new ProjectDto(
            "Project2",
            "",
            userDto.getId()
    );

    @NotNull
    private final ProjectDto projectTwo = new ProjectDto(
            "Project3",
            null,
            adminDto.getId()
    );

    @NotNull
    private final List<ProjectDto> allProjectInSystem = new ArrayList<>(Arrays.asList(projectOne, projectTwo));

    @NotNull
    private final TaskDto taskOne = new TaskDto(
            "Task2",
            null,
            userDto.getId()
    );

    @NotNull
    private final TaskDto taskTwo = new TaskDto(
            "Task3",
            null,
            adminDto.getId()
    );

    @NotNull
    private final List<TaskDto> allTaskInSystem = new ArrayList<>(Arrays.asList(taskOne, taskTwo));

    @NotNull
    private final SessionDto sessionOne = sessionService.setSignature(
            new SessionDto(
                    System.currentTimeMillis(),
                    userDto.getId(),
                    null
            )
    );

    @NotNull
    private final SessionDto sessionTwo = sessionService.setSignature(
            new SessionDto(
                    System.currentTimeMillis(),
                    adminDto.getId(),
                    null
            )
    );

    @NotNull
    private final List<SessionDto> allSessionInSystem = new ArrayList<>(Arrays.asList(sessionOne, sessionTwo));

    public DomainServiceTest() throws Exception {
        super();
    }

    private void loadData() {
        userService.saveAll(allUserInSystem);
        projectService.saveAll(allProjectInSystem);
        sessionService.saveAll(allSessionInSystem);
        taskService.saveAll(allTaskInSystem);
    }

    @After
    public void after() throws EmptyIdException {
        taskService.removeAll();
        projectService.removeAll();
        sessionService.removeAll();
        userService.removeOneById(userDto.getId());
        userService.removeOneById(adminDto.getId());
    }


    @Test
    public void testSaveData() throws EmptyDomainException {
        loadData();
        @NotNull final Domain domain = new Domain();

        domainService.saveData(domain);

        @NotNull final List<ProjectDto> projects = domain.getProjects();
        Assert.assertEquals(2, projects.size());
        for (@NotNull final ProjectDto project : projects)
            Assert.assertTrue(allProjectInSystem.contains(project));

        @NotNull final List<TaskDto> tasks = domain.getTasks();
        Assert.assertEquals(2, tasks.size());
        for (@NotNull final TaskDto task : tasks)
            Assert.assertTrue(allTaskInSystem.contains(task));

        @NotNull final List<SessionDto> sessions = domain.getSessions();
        Assert.assertEquals(2, sessions.size());
        for (@NotNull final SessionDto session : sessions)
            Assert.assertTrue(allSessionInSystem.contains(session));

        @NotNull final List<UserDto> users = domain.getUsers();
        Assert.assertEquals(4, users.size());
    }

    @Test(expected = EmptyDomainException.class)
    public void testSaveDataWithoutDomain() throws EmptyDomainException {
        domainService.saveData(null);
    }

    @Test
    public void testLoadData() throws AbstractException {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(allProjectInSystem);
        domain.setTasks(allTaskInSystem);
        domain.setUsers(allUserInSystem);
        domain.setSessions(allSessionInSystem);

        domainService.loadData(domain);
        @NotNull final List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertEquals(2, projects.size());
        for (@NotNull final ProjectDto project : projects)
            Assert.assertTrue(allProjectInSystem.contains(project));

        @NotNull final List<TaskDto> tasks = taskService.findAllDto();
        Assert.assertEquals(2, tasks.size());
        for (@NotNull final TaskDto task : tasks)
            Assert.assertTrue(allTaskInSystem.contains(task));

        @NotNull final List<SessionDto> sessions = sessionService.findAllDto();
        Assert.assertEquals(2, sessions.size());
        for (@NotNull final SessionDto session : sessions)
            Assert.assertTrue(allSessionInSystem.contains(session));

        @NotNull final List<UserDto> users = userService.findAllDto();
        Assert.assertEquals(4, users.size());
    }

    @Test(expected = EmptyDomainException.class)
    public void testLoadDataWithoutDomain() throws EmptyDomainException {
        domainService.loadData(null);
    }

}
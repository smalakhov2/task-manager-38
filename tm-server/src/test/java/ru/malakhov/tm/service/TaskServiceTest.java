package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.AbstractDataTest;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.dto.TaskDto;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.empty.EmptyNameException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;
import ru.malakhov.tm.exception.system.IndexIncorrectException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class TaskServiceTest extends AbstractDataTest {

    @NotNull
    final TaskDto unknownTask = new TaskDto(
            "Unknown",
            null,
            unknownUserDto.getId()
    );
    @NotNull
    private final ITaskService taskService = context.getBean(TaskService.class);
    @NotNull
    private final IUserService userService = context.getBean(UserService.class);
    @NotNull
    private final TaskDto taskOne = new TaskDto(
            "Task1",
            "",
            userDto.getId());
    @NotNull
    private final TaskDto taskTwo = new TaskDto(
            "Task2",
            "",
            userDto.getId()
    );
    @NotNull
    private final TaskDto taskThree = new TaskDto(
            "Task3",
            null,
            adminDto.getId()
    );
    @NotNull
    private final TaskDto taskFour = new TaskDto(
            "Task4",
            null,
            adminDto.getId()
    );
    @NotNull
    private final List<TaskDto> userTasks = new ArrayList<>(Arrays.asList(taskOne, taskTwo));

    @NotNull
    private final List<TaskDto> adminTasks = new ArrayList<>(Arrays.asList(taskThree, taskFour));

    public TaskServiceTest() throws Exception {
        super();
    }

    private void loadData() {
        taskService.saveAll(userTasks);
        taskService.saveAll(adminTasks);
    }

    @Before
    public void before() {
        userService.saveAll(userDto, adminDto);
    }

    @After
    public void after() throws EmptyIdException {
        taskService.removeAll();
        userService.removeOneById(userDto.getId());
        userService.removeOneById(adminDto.getId());
    }

    @Test
    public void testCreate() throws AbstractException {
        @NotNull final String userId = taskOne.getUserId();
        @NotNull final String taskName = taskOne.getName();

        taskService.create(userId, taskName);
        @Nullable final TaskDto task = taskService.findOneDtoByName(userId, taskName);
        Assert.assertNotNull(task);
        Assert.assertEquals(userId, task.getUserId());
        Assert.assertEquals(taskName, task.getName());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testCreateWithoutUserId() throws AbstractException {
        taskService.create(null, taskOne.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testCreateWithoutName() throws AbstractException {
        taskService.create(taskOne.getUserId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testCreateWithEmptyUserId() throws AbstractException {
        taskService.create("", taskOne.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testCreateWithEmptyName() throws AbstractException {
        taskService.create(taskOne.getUserId(), "");
    }

    @Test
    public void testCreateWithDescription() throws AbstractException {
        @NotNull final String userId = taskOne.getUserId();
        @NotNull final String taskName = taskOne.getName();
        @Nullable final String taskDescription = taskOne.getDescription();

        taskService.create(userId, taskName, taskDescription);
        @Nullable final TaskDto task = taskService.findOneDtoByName(userId, taskName);
        Assert.assertNotNull(task);
        Assert.assertEquals(userId, task.getUserId());
        Assert.assertEquals(taskName, task.getName());
        Assert.assertEquals(taskDescription, task.getDescription());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testCreateWithDescriptionWithoutUserId() throws AbstractException {
        taskService.create(null, taskOne.getName(), taskOne.getDescription());
    }

    @Test(expected = EmptyNameException.class)
    public void testCreateWithDescriptionWithoutName() throws AbstractException {
        taskService.create(taskOne.getUserId(), null, taskOne.getDescription());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testCreateWithDescriptionEmptyUserId() throws AbstractException {
        taskService.create("", taskOne.getName(), taskOne.getDescription());
    }

    @Test(expected = EmptyNameException.class)
    public void testCreateWithDescriptionEmptyName() throws AbstractException {
        taskService.create(taskOne.getUserId(), "", taskOne.getDescription());
    }

    @Test
    public void testFindAll() {
        loadData();
        @NotNull final List<TaskDto> tasks = taskService.findAllDto();
        Assert.assertEquals(4, tasks.size());
    }

    @Test
    public void testFindAllEntity() {
        loadData();
        @NotNull final List<Task> tasks = taskService.findAllEntity();
        Assert.assertEquals(4, tasks.size());
    }

    @Test
    public void testFindAllDtoByUserId() throws EmptyUserIdException {
        loadData();

        @NotNull final List<TaskDto> tasks = taskService.findAllDtoByUserId(userDto.getId());
        Assert.assertFalse(tasks.isEmpty());
        Assert.assertEquals(2, tasks.size());

        @NotNull final List<TaskDto> emptyList = taskService.findAllDtoByUserId(unknownUserDto.getId());
        Assert.assertTrue(emptyList.isEmpty());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindAllDtoByUserIdWithoutUserId() throws EmptyUserIdException {
        taskService.findAllDtoByUserId(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindAllDtoByUserIdWithEmptyUserId() throws EmptyUserIdException {
        taskService.findAllDtoByUserId("");
    }

    @Test
    public void testFindAllEntityByUserId() throws EmptyUserIdException {
        loadData();

        @NotNull final List<Task> tasks = taskService.findAllEntityByUserId(userDto.getId());
        Assert.assertFalse(tasks.isEmpty());
        Assert.assertEquals(2, tasks.size());

        @NotNull final List<Task> emptyList = taskService.findAllEntityByUserId(unknownUserDto.getId());
        Assert.assertTrue(emptyList.isEmpty());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindAllEntityByUserIdWithoutUserId() throws EmptyUserIdException {
        taskService.findAllEntityByUserId(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindAllEntityByUserIdWithEmptyUserId() throws EmptyUserIdException {
        taskService.findAllEntityByUserId("");
    }

    @Test
    public void testFindOneDtoById() throws EmptyIdException {
        loadData();

        @Nullable final TaskDto task = taskService.findOneDtoById(taskOne.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(taskOne.hashCode(), task.hashCode());
        Assert.assertEquals(taskOne, task);

        @Nullable final TaskDto nullTask = taskService.findOneDtoById(unknownTask.getId());
        Assert.assertNull(nullTask);
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneDtoByIdWithoutId() throws EmptyIdException {
        taskService.findOneDtoById(null);
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneDtoByIdWithEmptyId() throws EmptyIdException {
        taskService.findOneDtoById("");
    }

    @Test
    public void testFindOneEntityById() throws EmptyIdException {
        loadData();

        @Nullable final Task task = taskService.findOneEntityById(taskOne.getId());
        Assert.assertNotNull(task);

        @Nullable final Task nullTask = taskService.findOneEntityById(unknownTask.getId());
        Assert.assertNull(nullTask);
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneEntityByIdWithoutId() throws EmptyIdException {
        taskService.findOneEntityById(null);
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneEntityByIdWithEmptyId() throws EmptyIdException {
        taskService.findOneEntityById("");
    }

    @Test
    public void testFindOneDtoByIdAndUserId() throws AbstractException {
        loadData();

        @Nullable final TaskDto task =
                taskService.findOneDtoByIdAndUserId(taskOne.getId(), userDto.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(taskOne.hashCode(), task.hashCode());
        Assert.assertEquals(taskOne, task);

        @Nullable TaskDto nullTask =
                taskService.findOneDtoByIdAndUserId(unknownUserDto.getId(), unknownTask.getId());
        Assert.assertNull(nullTask);

        nullTask = taskService.findOneDtoByIdAndUserId(userDto.getId(), unknownTask.getId());
        Assert.assertNull(nullTask);

        nullTask = taskService.findOneDtoByIdAndUserId(unknownTask.getId(), taskOne.getId());
        Assert.assertNull(nullTask);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneDtoByIdAndUserIdWithoutUserId() throws AbstractException {
        taskService.findOneDtoByIdAndUserId(null, taskOne.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneDtoByIdAndUserIdWithoutId() throws AbstractException {
        taskService.findOneDtoByIdAndUserId(userDto.getId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneDtoByIdAndUserIdWithEmptyUserId() throws AbstractException {
        taskService.findOneDtoByIdAndUserId("", taskOne.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneDtoByIdAndUserIdWithEmptyId() throws AbstractException {
        taskService.findOneDtoByIdAndUserId(userDto.getId(), "");
    }

    @Test
    public void testFindOneEntityByIdAndUserId() throws AbstractException {
        loadData();

        @Nullable final Task task = taskService.findOneEntityByIdAndUserId(taskOne.getId(), userDto.getId());
        Assert.assertNotNull(task);

        @Nullable Task nullTask = taskService.findOneEntityByIdAndUserId(unknownUserDto.getId(), unknownTask.getId());
        Assert.assertNull(nullTask);

        nullTask = taskService.findOneEntityByIdAndUserId(userDto.getId(), unknownTask.getId());
        Assert.assertNull(nullTask);

        nullTask = taskService.findOneEntityByIdAndUserId(unknownUserDto.getId(), taskOne.getId());
        Assert.assertNull(nullTask);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneEntityByIdAndUserIdWithoutUserId() throws AbstractException {
        taskService.findOneEntityByIdAndUserId(null, taskOne.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneEntityByIdAndUserIdWithoutId() throws AbstractException {
        taskService.findOneEntityByIdAndUserId(userDto.getId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneEntityByIdAndUserIdWithEmptyUserId() throws AbstractException {
        taskService.findOneEntityByIdAndUserId("", taskOne.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneEntityByIdAndUserIdWithEmptyId() throws AbstractException {
        taskService.findOneEntityByIdAndUserId(userDto.getId(), "");
    }

    @Test
    public void testFindOneDtoByIndex() throws AbstractException {
        loadData();

        @Nullable final TaskDto task = taskService.findOneDtoByIndex(userDto.getId(), 0);
        Assert.assertNotNull(task);
        Assert.assertEquals(taskOne.hashCode(), task.hashCode());
        Assert.assertEquals(taskOne, task);

        @Nullable TaskDto nullTask = taskService.findOneDtoByIndex(unknownTask.getId(), 6);
        Assert.assertNull(nullTask);

        nullTask = taskService.findOneDtoByIndex(userDto.getId(), 6);
        Assert.assertNull(nullTask);

        nullTask = taskService.findOneDtoByIndex(unknownUserDto.getId(), 0);
        Assert.assertNull(nullTask);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneDtoByIndexWithoutUserId() throws AbstractException {
        taskService.findOneDtoByIndex(null, 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindOneDtoByIndexWithoutIndex() throws AbstractException {
        taskService.findOneDtoByIndex(userDto.getId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneDtoByIndexWithEmptyUserId() throws AbstractException {
        taskService.findOneDtoByIndex("", 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindOneDtoByIndexWithIncorrectIndex() throws AbstractException {
        taskService.findOneDtoByIndex(userDto.getId(), -3);
    }

    @Test
    public void testFindOneEntityByIndex() throws AbstractException {
        loadData();

        @Nullable final Task task = taskService.findOneEntityByIndex(userDto.getId(), 0);
        Assert.assertNotNull(task);

        @Nullable Task nullTask = taskService.findOneEntityByIndex(unknownTask.getId(), 6);
        Assert.assertNull(nullTask);

        nullTask = taskService.findOneEntityByIndex(userDto.getId(), 6);
        Assert.assertNull(nullTask);

        nullTask = taskService.findOneEntityByIndex(unknownUserDto.getId(), 0);
        Assert.assertNull(nullTask);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneEntityByIndexWithoutUserId() throws AbstractException {
        taskService.findOneEntityByIndex(null, 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindOneEntityByIndexWithoutIndex() throws AbstractException {
        taskService.findOneEntityByIndex(userDto.getId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneEntityByIndexWithEmptyUserId() throws AbstractException {
        taskService.findOneEntityByIndex("", 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindOneEntityByIndexWithIncorrectIndex() throws AbstractException {
        taskService.findOneEntityByIndex(userDto.getId(), -3);
    }

    @Test
    public void testFindOneDtoByName() throws AbstractException {
        loadData();

        @Nullable final TaskDto task = taskService.findOneDtoByName(userDto.getId(), taskOne.getName());
        Assert.assertNotNull(task);
        Assert.assertEquals(taskOne.hashCode(), task.hashCode());
        Assert.assertEquals(taskOne, task);

        @Nullable TaskDto nullTask = taskService.findOneDtoByName(unknownTask.getId(), unknownTask.getName());
        Assert.assertNull(nullTask);

        nullTask = taskService.findOneDtoByName(userDto.getId(), unknownTask.getName());
        Assert.assertNull(nullTask);

        nullTask = taskService.findOneDtoByName(unknownUserDto.getId(), taskOne.getName());
        Assert.assertNull(nullTask);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneDtoByNameWithoutUserId() throws AbstractException {
        taskService.findOneDtoByName(null, taskOne.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testFindOneDtoByNameWithoutName() throws AbstractException {
        taskService.findOneDtoByName(userDto.getId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneDtoByNameWithEmptyUserId() throws AbstractException {
        taskService.findOneDtoByName("", taskOne.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testFindOneDtoByNameWithEmptyName() throws AbstractException {
        taskService.findOneDtoByName(userDto.getId(), "");
    }

    @Test
    public void testFindOneEntityByName() throws AbstractException {
        loadData();

        @Nullable final Task task = taskService.findOneEntityByName(userDto.getId(), taskOne.getName());
        Assert.assertNotNull(task);

        @Nullable Task nullTask = taskService.findOneEntityByName(unknownTask.getId(), unknownTask.getName());
        Assert.assertNull(nullTask);

        nullTask = taskService.findOneEntityByName(userDto.getId(), unknownTask.getName());
        Assert.assertNull(nullTask);

        nullTask = taskService.findOneEntityByName(unknownUserDto.getId(), taskOne.getName());
        Assert.assertNull(nullTask);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneEntityByNameWithoutUserId() throws AbstractException {
        taskService.findOneEntityByName(null, taskOne.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testFindOneEntityByNameWithoutName() throws AbstractException {
        taskService.findOneEntityByName(userDto.getId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneEntityByNameWithEmptyUserId() throws AbstractException {
        taskService.findOneEntityByName("", taskOne.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testFindOneEntityByNameWithEmptyName() throws AbstractException {
        taskService.findOneEntityByName(userDto.getId(), "");
    }

    @Test
    public void testRemoveAll() {
        loadData();

        taskService.removeAll();
        @NotNull final List<TaskDto> tasks = taskService.findAllDto();
        Assert.assertTrue(tasks.isEmpty());
    }

    @Test
    public void testRemoveAllByUserId() throws EmptyUserIdException {
        loadData();

        taskService.removeAllByUserId(userDto.getId());
        @NotNull List<TaskDto> tasks = taskService.findAllDtoByUserId(userDto.getId());
        Assert.assertTrue(tasks.isEmpty());

        taskService.removeAllByUserId(unknownUserDto.getId());
        tasks = taskService.findAllDto();
        Assert.assertEquals(2, tasks.size());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveAllByUserIdWithoutUserId() throws EmptyUserIdException {
        taskService.findAllDtoByUserId(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveAllByUserIdWithEmptyUserId() throws EmptyUserIdException {
        taskService.findAllDtoByUserId(null);
    }

    @Test
    public void testRemoveOneById() throws EmptyIdException {
        loadData();

        taskService.removeOneById(taskOne.getId());
        @Nullable final TaskDto task = taskService.findOneDtoById(taskOne.getId());
        Assert.assertNull(task);

        taskService.removeOneById(unknownTask.getId());
        @NotNull final List<TaskDto> tasks = taskService.findAllDto();
        Assert.assertEquals(3, tasks.size());
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveOneByIdWithoutId() throws EmptyIdException {
        taskService.removeOneById(null);
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveOneByIdWithEmptyId() throws EmptyIdException {
        taskService.removeOneById("");
    }

    @Test
    public void testRemoveOneByIdAndUserId() throws AbstractException {
        loadData();

        taskService.removeOneByIdAndUserId(taskOne.getId(), userDto.getId());
        @Nullable final TaskDto task = taskService.findOneDtoById(taskOne.getId());
        Assert.assertNull(task);

        taskService.removeOneByIdAndUserId(unknownUserDto.getId(), unknownTask.getId());
        @NotNull List<TaskDto> tasks = taskService.findAllDto();
        Assert.assertEquals(3, tasks.size());

        taskService.removeOneByIdAndUserId(userDto.getId(), unknownTask.getId());
        tasks = taskService.findAllDto();
        Assert.assertEquals(3, tasks.size());

        taskService.removeOneByIdAndUserId(unknownUserDto.getId(), taskOne.getId());
        tasks = taskService.findAllDto();
        Assert.assertEquals(3, tasks.size());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveOneByIdAndUserIdIdWithoutUserId() throws AbstractException {
        taskService.removeOneByIdAndUserId(null, taskOne.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveOneEntityByIdAndUserIdWithoutId() throws AbstractException {
        taskService.removeOneByIdAndUserId(userDto.getId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveOneEntityByIdAndUserIdWithEmptyUserId() throws AbstractException {
        taskService.removeOneByIdAndUserId("", taskOne.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveOneEntityByIdAndUserIdWithEmptyId() throws AbstractException {
        taskService.removeOneByIdAndUserId(userDto.getId(), "");
    }

    @Test
    public void testRemoveOneByIndex() throws AbstractException {
        loadData();

        taskService.removeOneByIndex(userDto.getId(), 0);
        @Nullable final TaskDto task = taskService.findOneDtoById(taskOne.getId());
        Assert.assertNull(task);

        taskService.removeOneByIndex(unknownUserDto.getId(), 6);
        @NotNull List<TaskDto> tasks = taskService.findAllDto();
        Assert.assertEquals(3, tasks.size());

        taskService.removeOneByIndex(userDto.getId(), 6);
        tasks = taskService.findAllDto();
        Assert.assertEquals(3, tasks.size());

        taskService.removeOneByIndex(unknownUserDto.getId(), 0);
        tasks = taskService.findAllDto();
        Assert.assertEquals(3, tasks.size());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveOneEntityByIndexWithoutUserId() throws AbstractException {
        taskService.removeOneByIndex(null, 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveOneEntityByIndexWithoutIndex() throws AbstractException {
        taskService.removeOneByIndex(userDto.getId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveOneEntityByIndexWithEmptyUserId() throws AbstractException {
        taskService.removeOneByIndex("", 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveOneEntityByIndexWithIncorrectIndex() throws AbstractException {
        taskService.removeOneByIndex(userDto.getId(), -3);
    }

    @Test
    public void testRemoveOneByName() throws AbstractException {
        loadData();

        taskService.removeOneByName(userDto.getId(), taskOne.getName());
        @Nullable final TaskDto task = taskService.findOneDtoById(taskOne.getId());
        Assert.assertNull(task);

        taskService.removeOneByName(unknownUserDto.getId(), unknownTask.getName());
        @NotNull List<TaskDto> tasks = taskService.findAllDto();
        Assert.assertEquals(3, tasks.size());

        taskService.removeOneByName(userDto.getId(), unknownTask.getName());
        tasks = taskService.findAllDto();
        Assert.assertEquals(3, tasks.size());

        taskService.removeOneByName(unknownUserDto.getId(), taskOne.getName());
        tasks = taskService.findAllDto();
        Assert.assertEquals(3, tasks.size());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveOneEntityByNameWithoutUserId() throws AbstractException {
        taskService.removeOneByName(null, taskOne.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testRemoveOneEntityByNameWithoutName() throws AbstractException {
        taskService.removeOneByName(userDto.getId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveOneEntityByNameWithEmptyUserId() throws AbstractException {
        taskService.removeOneByName("", taskOne.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testRemoveOneEntityByNameWithEmptyName() throws AbstractException {
        taskService.removeOneByName(userDto.getId(), "");
    }

    @Test
    public void testUpdateById() throws AbstractException {
        loadData();
        @NotNull final String unknownUserId = unknownTask.getUserId();
        @NotNull final String unknownId = unknownTask.getId();
        @NotNull final String userId = taskOne.getUserId();
        @NotNull final String id = taskOne.getId();
        @NotNull final String newName = "New name";
        @NotNull final String newDescription = "New description";

        taskService.updateTaskById(
                userId,
                id,
                newName,
                newDescription
        );
        @Nullable final TaskDto task = taskService.findOneDtoById(id);
        Assert.assertNotNull(task);
        Assert.assertEquals(newName, task.getName());
        Assert.assertEquals(newDescription, task.getDescription());

        taskService.updateTaskById(
                unknownUserId,
                unknownId,
                newName,
                newDescription
        );
        @Nullable final TaskDto nullTask = taskService.findOneDtoByIdAndUserId(unknownId, unknownUserId);
        Assert.assertNull(nullTask);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdateByIdWithoutUserId() throws AbstractException {
        taskService.updateTaskById(
                null,
                taskOne.getId(),
                taskOne.getName(),
                taskOne.getDescription()
        );
    }

    @Test(expected = EmptyIdException.class)
    public void testUpdateByIdWithoutId() throws AbstractException {
        taskService.updateTaskById(
                taskOne.getUserId(),
                null,
                taskOne.getName(),
                taskOne.getDescription()
        );
    }

    @Test(expected = EmptyNameException.class)
    public void testUpdateByIdWithoutName() throws AbstractException {
        taskService.updateTaskById(
                taskOne.getUserId(),
                taskOne.getId(),
                null,
                taskOne.getDescription()
        );
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdateByIdWithEmptyUserId() throws AbstractException {
        taskService.updateTaskById(
                "",
                taskOne.getId(),
                taskOne.getName(),
                taskOne.getDescription()
        );
    }

    @Test(expected = EmptyIdException.class)
    public void testUpdateByIdWithEmptyId() throws AbstractException {
        taskService.updateTaskById(
                taskOne.getUserId(),
                "",
                taskOne.getName(),
                taskOne.getDescription()
        );
    }

    @Test(expected = EmptyNameException.class)
    public void testUpdateByIdWithEmptyName() throws AbstractException {
        taskService.updateTaskById(
                taskOne.getUserId(),
                taskOne.getId(),
                "",
                taskOne.getDescription()
        );
    }

    @Test
    public void testUpdateByIndex() throws AbstractException {
        loadData();
        @NotNull final String unknownUserId = unknownUserDto.getId();
        @NotNull final String userId = userDto.getId();
        final int index = 0;
        @NotNull final String newName = "New name1";
        @NotNull final String newDescription = "New description1";

        taskService.updateTaskByIndex(
                userId,
                index,
                newName,
                newDescription
        );
        @Nullable final TaskDto task = taskService.findOneDtoByIndex(userId, index);
        Assert.assertNotNull(task);
        Assert.assertEquals(newName, task.getName());
        Assert.assertEquals(newDescription, task.getDescription());

        taskService.updateTaskByIndex(
                unknownUserId,
                5,
                newName,
                newDescription
        );
        @Nullable final TaskDto nullTask = taskService.findOneDtoByIndex(unknownUserId, 5);
        Assert.assertNull(nullTask);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdateByIndexWithoutUserId() throws AbstractException {
        loadData();
        taskService.updateTaskByIndex(
                null,
                0,
                taskOne.getName(),
                taskOne.getDescription()
        );
    }

    @Test(expected = EmptyNameException.class)
    public void testUpdateByIndexWithoutName() throws AbstractException {
        loadData();
        taskService.updateTaskByIndex(
                taskOne.getUserId(),
                0,
                null,
                taskOne.getDescription()
        );
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdateByIndexWithEmptyUserId() throws AbstractException {
        loadData();
        taskService.updateTaskByIndex(
                "",
                0,
                taskOne.getName(),
                taskOne.getDescription()
        );
    }

    @Test(expected = EmptyNameException.class)
    public void testUpdateByIndexWithEmptyName() throws AbstractException {
        loadData();
        taskService.updateTaskByIndex(
                taskOne.getUserId(),
                0,
                "",
                taskOne.getDescription()
        );
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIndexWithIndexLessRange() throws AbstractException {
        taskService.updateTaskByIndex(
                taskOne.getUserId(),
                -1,
                taskOne.getName(),
                taskOne.getDescription()
        );
    }

}
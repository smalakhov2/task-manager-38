package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.AbstractDataTest;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.dto.ProjectDto;
import ru.malakhov.tm.exception.empty.EmptyIdException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class AbstractServiceTest extends AbstractDataTest {

    @NotNull
    private final IProjectService projectService = context.getBean(ProjectService.class);

    @NotNull
    private final IUserService userService = context.getBean(UserService.class);

    @NotNull
    private final ProjectDto projectOne = new ProjectDto("Project1", "", userDto.getId());

    @NotNull
    private final ProjectDto projectTwo = new ProjectDto("Project2", "", userDto.getId());

    @NotNull
    private final ProjectDto projectThree = new ProjectDto("Project3", "", userDto.getId());

    @NotNull
    private final ProjectDto unknownProject = new ProjectDto("Unknown", "", unknownUserDto.getId());

    @NotNull
    private final List<ProjectDto> allProjects = new ArrayList<>(Arrays.asList(projectOne, projectTwo, projectThree));

    public AbstractServiceTest() throws Exception {
        super();
    }

    private void loadData() {
        projectService.saveAll(allProjects);
    }

    @Before
    public void before() {
        userService.saveAll(userDto, adminDto);
    }

    @After
    public void after() throws EmptyIdException {
        projectService.removeAll();
        userService.removeOneById(userDto.getId());
        userService.removeOneById(adminDto.getId());
    }

    @Test
    public void testSave() {
        @Nullable final ProjectDto nullProject = null;
        projectService.save(nullProject);
        @NotNull List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertTrue(projects.isEmpty());

        projectService.save(projectOne);
        projects = projectService.findAllDto();
        Assert.assertEquals(1, projects.size());
        Assert.assertEquals(projectOne, projects.get(0));
    }

    @Test
    public void testSaveAll() {
        @Nullable final List<ProjectDto> nullCollection = null;
        projectService.saveAll(nullCollection);
        @NotNull List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertTrue(projects.isEmpty());

        projectService.saveAll(allProjects);
        projects = projectService.findAllDto();
        Assert.assertEquals(3, projects.size());
    }

}
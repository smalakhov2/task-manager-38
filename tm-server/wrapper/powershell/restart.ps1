write-host "RESTART SERVER...";

if ( -not ( Test-Path "server.pid" ) ) {
    write-host "server.pid not found!"
    Wait-Event -Timeout 5
    exit 1;
}

Start-Process powershell  -ArgumentList "-ExecutionPolicy bypass -File shutdown.ps1" -NoNewWindow -Wait
write-host ""
Start-Process powershell  -ArgumentList "-ExecutionPolicy bypass -File startup.ps1"  -NoNewWindow
Wait-Event -Timeout 5
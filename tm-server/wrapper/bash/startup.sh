#!/usr/bin/env bash

echo "STARTING SERVER..."

if [ -f ./server.pid ]; then
  echo "Application already started!"
  exit 1;
fi

mkdir -p ./log
rm -f ./log/server.log
nohup java -jar ./tm-server.jar > ./log/server.log 2>&1 &
echo $! > server.pid
echo "OK"
package ru.malakhov.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.format.datetime.DateFormatterRegistrar;
import org.springframework.format.datetime.standard.DateTimeFormatterRegistrar;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import ru.malakhov.tm.constant.FormatConstant;

import java.time.format.DateTimeFormatter;

@EnableWebMvc
@Configuration
@ComponentScan("ru.malakhov.tm")
public class WebMvcConfig extends WebMvcConfigurationSupport {

    @Bean
    @NotNull
    public InternalResourceViewResolver resolver() {
        @NotNull final InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

    @Bean
    @NotNull
    @Override
    public FormattingConversionService mvcConversionService() {
        @NotNull final DefaultFormattingConversionService conversionService = new DefaultFormattingConversionService(false);
        @NotNull final DateTimeFormatterRegistrar dateTimeRegistrar = new DateTimeFormatterRegistrar();
        dateTimeRegistrar.setDateFormatter(DateTimeFormatter.ofPattern(FormatConstant.DATE_FORMAT));
        dateTimeRegistrar.setDateTimeFormatter(DateTimeFormatter.ofPattern(FormatConstant.DATE_TIME_FORMAT));
        dateTimeRegistrar.registerFormatters(conversionService);
        @NotNull final DateFormatterRegistrar dateRegistrar = new DateFormatterRegistrar();
        dateRegistrar.setFormatter(new DateFormatter(FormatConstant.DATE_FORMAT));
        dateRegistrar.registerFormatters(conversionService);
        return conversionService;
    }

    @Override
    public void addViewControllers(@NotNull final ViewControllerRegistry registry) {
    }

    @Override
    public void addResourceHandlers(@NotNull final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/css/**").addResourceLocations("/WEB-INF/css/");
    }

}
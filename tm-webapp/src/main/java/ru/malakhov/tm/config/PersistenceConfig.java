package ru.malakhov.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
@EnableJpaRepositories("ru.malakhov.tm.repository")
public final class PersistenceConfig {

    @NotNull
    private final Environment env;

    @Autowired
    public PersistenceConfig(@NotNull final Environment env) {
        this.env = env;
    }

    @Bean
    @NotNull
    public DataSource dataSource() {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("jdbc.driver", "com.mysql.jdbc.Driver"));
        dataSource.setUrl(
                env.getProperty(
                        "jdbc.url",
                        "jdbc:mysql://localhost:3306/task-manager?zeroDateTimeBehavior=convertToNull"
                )
        );
        dataSource.setUsername(env.getProperty("jdbc.username", "root"));
        dataSource.setPassword(env.getProperty("jdbc.password", "root"));
        return dataSource;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final DataSource dataSource
    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean =
                new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.malakhov.tm.entity", "ru.malakhov.tm.dto");
        factoryBean.setJpaProperties(additionalProperties());
        return factoryBean;
    }

    @NotNull
    private Properties additionalProperties() {
        @NotNull final Properties hibernateProperties = new Properties();
        hibernateProperties.put(
                "hibernate.show_sql",
                env.getProperty(
                        "hibernate.show_sql",
                        "false"
                )
        );
        hibernateProperties.put(
                "hibernate.hbm2ddl.auto",
                env.getProperty(
                        "hibernate.hbm2ddl.auto",
                        "update"
                )
        );
        hibernateProperties.put(
                "hibernate.dialect",
                env.getProperty(
                        "hibernate.dialect",
                        "org.hibernate.dialect.MySQL5InnoDBDialect"
                )
        );
        return hibernateProperties;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

}
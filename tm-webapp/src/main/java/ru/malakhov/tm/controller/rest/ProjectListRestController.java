package ru.malakhov.tm.controller.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.malakhov.tm.api.service.dto.IProjectDtoService;
import ru.malakhov.tm.dto.ProjectDto;

import java.util.List;

@RestController
@RequestMapping("/api/v1/projects")
public class ProjectListRestController {

    @NotNull
    @Autowired
    private IProjectDtoService projectDtoService;

    @PostMapping
    public void createProjects(@RequestBody @Nullable final List<ProjectDto> entities) {
        projectDtoService.saveAll(entities);
    }

    @PutMapping
    public void updateProjectsPut(@RequestBody @Nullable final List<ProjectDto> entities) {
        projectDtoService.saveAll(entities);
    }

    @PatchMapping
    public void updateProjectsPatch(@RequestBody @Nullable final List<ProjectDto> entities) {
        projectDtoService.saveAll(entities);
    }

    @DeleteMapping
    public void deleteProjects(@RequestBody @Nullable final List<ProjectDto> entities) {
        projectDtoService.deleteAll(entities);
    }

    @DeleteMapping("/all")
    public void deleteAllProjects() {
        projectDtoService.deleteAll();
    }

    @NotNull
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProjectDto> findAllProjects() {
        return projectDtoService.findAll();
    }

    @GetMapping(value = "/count", produces = MediaType.APPLICATION_JSON_VALUE)
    public long countProjects() {
        return projectDtoService.count();
    }

}
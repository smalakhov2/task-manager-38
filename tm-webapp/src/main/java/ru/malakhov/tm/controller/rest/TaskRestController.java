package ru.malakhov.tm.controller.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.malakhov.tm.api.service.dto.ITaskDtoService;
import ru.malakhov.tm.dto.TaskDto;

@RestController
@RequestMapping("/api/v1/task")
public class TaskRestController {

    @NotNull
    @Autowired
    private ITaskDtoService taskDtoService;

    @PostMapping
    public void createTask(@RequestBody @Nullable final TaskDto task) {
        taskDtoService.save(task);
    }

    @PutMapping
    public void updateTaskPut(@RequestBody @Nullable final TaskDto task) {
        taskDtoService.save(task);
    }

    @PatchMapping
    public void updateTaskPatch(@RequestBody @Nullable final TaskDto task) {
        taskDtoService.save(task);
    }

    @Nullable
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskDto findTaskById(
            @Nullable @PathVariable("id") final String id
    ) {
        if (id == null || id.isEmpty()) return null;
        return taskDtoService.findOneById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteTaskById(
            @Nullable @PathVariable("id") final String id
    ) {
        if (id == null || id.isEmpty()) return;
        taskDtoService.deleteOneById(id);
    }

    @GetMapping(value = "/exists/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean existTaskById(
            @Nullable @PathVariable("id") final String id
    ) {
        return taskDtoService.existById(id);
    }

}
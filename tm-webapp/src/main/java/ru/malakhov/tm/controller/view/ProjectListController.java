package ru.malakhov.tm.controller.view;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.malakhov.tm.api.service.dto.IProjectDtoService;

@Controller
@RequestMapping("/projects")
public class ProjectListController {

    @NotNull
    @Autowired
    private IProjectDtoService projectDtoService;

    @GetMapping
    public ModelAndView viewProjectListGet() {
        return new ModelAndView("project_list", "projects", projectDtoService.findAll());
    }

}

package ru.malakhov.tm.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.constant.FormatConstant;
import ru.malakhov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_task")
public final class TaskDto extends AbstractEntityDto {

    public static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "name")
    private String name = "";

    @Nullable
    @Column(name = "description")
    private String description = "";

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    @NotNull
    @Column(name = "status")
    @Enumerated(value = EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "date_start")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = FormatConstant.DATE_TIME_FORMAT)
    private Date dateStart;

    @Nullable
    @Column(name = "date_finish")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = FormatConstant.DATE_TIME_FORMAT)
    private Date dateFinish;

    public TaskDto(@NotNull final String name) {
        this.name = name;
    }

    public TaskDto(@NotNull final String name, @Nullable final String description) {
        this.name = name;
        this.description = description;
    }

    public TaskDto(
            @NotNull final String name,
            @Nullable final String description,
            @NotNull final String userId
    ) {
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o instanceof TaskDto && super.equals(o)) {
            @NotNull final TaskDto task = (TaskDto) o;
            return Objects.equals(name, task.name)
                    && Objects.equals(description, task.description)
                    && Objects.equals(projectId, task.projectId);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, projectId);
    }

}
package ru.malakhov.tm.service.entity;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.malakhov.tm.api.service.entity.ITaskService;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.repository.entity.TaskRepository;

@Setter
@Service
public class TaskService extends AbstractService<Task, TaskRepository> implements ITaskService {

    @Autowired
    public TaskService(@NotNull final TaskRepository taskRepository) {
        super(taskRepository);
    }

}
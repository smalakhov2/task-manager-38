package ru.malakhov.tm.service.dto;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.malakhov.tm.api.service.dto.IProjectDtoService;
import ru.malakhov.tm.dto.ProjectDto;
import ru.malakhov.tm.repository.dto.ProjectDtoRepository;

@Setter
@Service
public class ProjectDtoService extends AbstractDtoService<ProjectDto, ProjectDtoRepository> implements IProjectDtoService {

    @Autowired
    public ProjectDtoService(@NotNull final ProjectDtoRepository projectDtoRepository) {
        super(projectDtoRepository);
    }

    public void create() {
        repository.save(new ProjectDto("New project"));
    }

}
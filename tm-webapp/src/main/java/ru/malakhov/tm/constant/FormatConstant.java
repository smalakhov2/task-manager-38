package ru.malakhov.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class FormatConstant {

    @NotNull
    public static final String DATE_FORMAT = "yyyy-MM-dd";

    @NotNull
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

}
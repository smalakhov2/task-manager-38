package ru.malakhov.tm.api.service.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.AbstractEntity;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.repository.entity.AbstractEntityRepository;

import java.util.Collection;
import java.util.List;

public interface IService<E extends AbstractEntity, R extends AbstractEntityRepository<E>> {

    void save(@Nullable E entity);

    void saveAll(@Nullable Collection<E> entities);

    void saveAll(@Nullable E... entities);

    @NotNull
    List<E> findAll();

    @Nullable
    E findOneById(@Nullable String id);

    boolean existById(@Nullable String id) throws EmptyIdException;

    long count();

    void deleteById(@Nullable String id);

    void delete(@Nullable E entity);

    void deleteAll(@Nullable Collection<E> entities);

    void deleteAll(@Nullable E... entities);

    void deleteAll();

}
package ru.malakhov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.dto.AbstractEntityDto;
import ru.malakhov.tm.repository.dto.AbstractDtoRepository;

import java.util.Collection;
import java.util.List;

public interface IDtoService<E extends AbstractEntityDto, R extends AbstractDtoRepository<E>> {

    void save(@Nullable E entity);

    void saveAll(@Nullable Collection<E> entities);

    @Transactional
    public void saveAll(@Nullable E... entities);

    @NotNull
    List<E> findAll();

    @Nullable
    E findOneById(@NotNull String id);

    void deleteOneById(@Nullable String id);

    void delete(@Nullable E entity);

    void deleteAll(@Nullable Collection<E> entities);

    void deleteAll(@Nullable E... entities);

    void deleteAll();

    boolean existById(@Nullable String id);

    long count();

}
package ru.malakhov.tm.repository.entity;

import org.springframework.stereotype.Repository;
import ru.malakhov.tm.entity.Project;

@Repository
public interface ProjectRepository extends AbstractEntityRepository<Project> {
}
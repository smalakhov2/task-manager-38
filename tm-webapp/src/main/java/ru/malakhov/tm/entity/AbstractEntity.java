package ru.malakhov.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

    @Id
    @NotNull
    protected String id = UUID.randomUUID().toString();

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o instanceof AbstractEntity) {
            @NotNull final AbstractEntity abstractEntity = (AbstractEntity) o;
            return id.equals(abstractEntity.getId());
        }
        return false;
    }

    public @NotNull String getId() {
        return id;
    }
}
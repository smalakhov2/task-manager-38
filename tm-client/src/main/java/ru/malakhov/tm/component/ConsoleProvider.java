package ru.malakhov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.component.IConsoleProvider;
import ru.malakhov.tm.exception.system.NumberIncorrectException;

import java.util.Scanner;

@Component
public final class ConsoleProvider implements IConsoleProvider {

    @NotNull
    private final Scanner SCANNER = new Scanner(System.in);

    @NotNull
    @Override
    public String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    @Override
    public Integer nextNumber() throws NumberIncorrectException {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new NumberIncorrectException(value, e);
        }
    }

}

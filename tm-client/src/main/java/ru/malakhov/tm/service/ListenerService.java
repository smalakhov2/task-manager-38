package ru.malakhov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.api.repository.IListenerRepository;
import ru.malakhov.tm.api.service.IListenerService;
import ru.malakhov.tm.listener.AbstractListener;

import java.util.Map;
import java.util.Set;

@Repository
@NoArgsConstructor
public class ListenerService implements IListenerService {

    @NotNull
    @Autowired
    private IListenerRepository listenerRepository;

    public @NotNull Map<String, AbstractListener> getCommands() {
        return listenerRepository.getCommands();
    }

    public @NotNull Set<String> getCommandsName() {
        return listenerRepository.getCommandsName();
    }

    public @NotNull Set<String> getCommandsArg() {
        return listenerRepository.getCommandsArg();
    }
}
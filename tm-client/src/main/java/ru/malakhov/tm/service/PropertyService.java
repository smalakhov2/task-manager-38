package ru.malakhov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.malakhov.tm.api.repository.IPropertyRepository;
import ru.malakhov.tm.api.service.IPropertyService;
import ru.malakhov.tm.endpoint.SessionDto;

@Service
@NoArgsConstructor
public final class PropertyService implements IPropertyService {

    @NotNull
    @Autowired
    private IPropertyRepository propertyRepository;

    @Nullable
    @Override
    public SessionDto getSession() {
        return propertyRepository.getSession();
    }

    @Override
    public void setSession(@Nullable final SessionDto session) {
        propertyRepository.setSession(session);
    }

    @Override
    public boolean isAuth() {
        return getSession() != null;
    }

}
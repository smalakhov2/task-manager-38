package ru.malakhov.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.api.repository.IListenerRepository;
import ru.malakhov.tm.listener.AbstractListener;

import javax.annotation.PostConstruct;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
@Repository
@NoArgsConstructor
public final class ListenerRepository implements IListenerRepository {

    @NotNull
    @Autowired
    private AbstractListener[] commandList;

    @NotNull
    private Map<String, AbstractListener> commands = new LinkedHashMap<>();

    @PostConstruct
    private void init() {
        for (@NotNull final AbstractListener command : commandList) {
            commands.put(command.name(), command);
        }
    }

    @NotNull
    @Override
    public Map<String, AbstractListener> getCommands() {
        return commands;
    }

    @NotNull
    @Override
    public Set<String> getCommandsName() {
        return commands.keySet();
    }

    @NotNull
    @Override
    public Set<String> getCommandsArg() {
        @NotNull final Set<String> args = new LinkedHashSet<>();
        for (@Nullable final AbstractListener command : this.commands.values()) {
            if (command == null) continue;
            @Nullable final String arg = command.arg();
            if (arg == null || arg.isEmpty()) continue;
            args.add(arg);
        }
        return args;
    }

}
package ru.malakhov.tm.listener.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.component.IConsoleProvider;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.Result;
import ru.malakhov.tm.endpoint.UserEndpoint;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;

@Component
public class RegistryListener extends AbstractListener {

    @NotNull
    @Autowired
    private IConsoleProvider consoleProvider;

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Override
    public String name() {
        return "registry";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Registry.";
    }

    @Override
    @EventListener(condition = "@registryListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws AbstractException_Exception {
        System.out.println("[REGISTRY]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = consoleProvider.nextLine();
        System.out.print("ENTER PASSWORD: ");
        @NotNull final String password = consoleProvider.nextLine();
        System.out.print("ENTER EMAIL: ");
        @NotNull final String email = consoleProvider.nextLine();
        @NotNull final Result result = userEndpoint.registryUser(login, password, email);
        if (result.isSuccess()) {
            System.out.println("[OK]");
        } else System.out.println("[FAIL]");
    }

    @Override
    public boolean secure() {
        return false;
    }

}

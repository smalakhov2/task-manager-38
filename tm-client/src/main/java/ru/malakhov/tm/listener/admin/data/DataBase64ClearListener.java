package ru.malakhov.tm.listener.admin.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.AdminDataEndpoint;
import ru.malakhov.tm.endpoint.Result;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;
import ru.malakhov.tm.service.PropertyService;

import java.io.IOException;

@Component
@NoArgsConstructor
public class DataBase64ClearListener extends AbstractListener {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private AdminDataEndpoint adminDataEndpoint;

    @NotNull
    @Override
    public String name() {
        return "data-base64-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove Base64 data.";
    }

    @Override
    @EventListener(condition = "@dataBase64ClearListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws IOException, AbstractException_Exception {
        System.out.println("[DATA BASE64 CLEAR]");
        @Nullable final SessionDto session = propertyService.getSession();
        @NotNull final Result result = adminDataEndpoint.clearDataBase64(session);
        if (result.isSuccess()) System.out.println("[OK]");
        else {
            System.out.println("MESSAGE" + result.getMessage());
            System.out.println("[FAIL]");
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}

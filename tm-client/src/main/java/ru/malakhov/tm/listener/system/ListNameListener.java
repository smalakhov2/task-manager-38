package ru.malakhov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.service.IListenerService;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.event.RunEvent;
import ru.malakhov.tm.listener.AbstractListener;
import ru.malakhov.tm.service.ListenerService;

import java.util.Set;

@Component
public class ListNameListener extends AbstractListener {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program commands.";
    }

    private void execute() {
        System.out.println("[COMMANDS]");
        @NotNull final IListenerService listenerService = context.getBean(ListenerService.class);
        @NotNull final Set<String> commands = listenerService.getCommandsName();
        for (@NotNull final String command : commands) System.out.println(command);
    }

    @Override
    @EventListener(condition = "@listNameListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        execute();
    }

    @EventListener(condition = "@listNameListener.arg() == #event.name")
    public void handler(@NotNull final RunEvent event) {
        execute();
    }

    @Override
    public boolean secure() {
        return false;
    }

}

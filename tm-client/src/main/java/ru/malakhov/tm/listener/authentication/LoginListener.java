package ru.malakhov.tm.listener.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.component.IConsoleProvider;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.endpoint.SessionEndpoint;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;
import ru.malakhov.tm.service.PropertyService;

@Component
public class LoginListener extends AbstractListener {

    @NotNull
    @Autowired
    private IConsoleProvider consoleProvider;

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Login in system.";
    }

    @Override
    @EventListener(condition = "@loginListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws AbstractException_Exception {
        System.out.println("[LOGIN]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = consoleProvider.nextLine();
        System.out.print("ENTER PASSWORD: ");
        @NotNull final String password = consoleProvider.nextLine();
        @Nullable SessionDto session = sessionEndpoint.openSession(login, password);
        if (session == null) {
            System.out.println("[ACCESS DENIED]");
            return;
        }
        propertyService.setSession(session);
        System.out.println("[OK]");
    }


    @Override
    public boolean secure() {
        return false;
    }

}

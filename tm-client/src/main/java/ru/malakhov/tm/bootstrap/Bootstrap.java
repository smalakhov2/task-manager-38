package ru.malakhov.tm.bootstrap;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.component.IConsoleProvider;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.event.RunEvent;


@Component
@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    @Autowired
    private IConsoleProvider consoleProvider;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    private void parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        for (@Nullable final String arg : args) {
            try {
                parseArg(arg);
            } catch (@NotNull final Exception e) {
                logError(e);
            }
        }
    }

    private void parseArg(@Nullable final String arg) throws Exception {
        if (arg == null || arg.isEmpty()) return;
        publisher.publishEvent(new RunEvent(arg));
    }

    private void parseCommand(@Nullable final String cmd) throws Exception {
        if (cmd == null || cmd.isEmpty()) return;
        publisher.publishEvent(new ConsoleEvent(cmd));
    }

    public void run(@NotNull final String[] args) {
        parseArgs(args);
        displayWelcome();
        while (true) {
            try {
                parseCommand(consoleProvider.nextLine());
            } catch (@NotNull final Exception e) {
                logError(e);
            }
        }
    }

    private void logError(@NotNull Exception e) {
        System.err.println(e.getMessage());
        System.err.println("[FAIL]");
    }

    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
        System.out.println();
    }

}
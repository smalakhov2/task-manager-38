package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.category.IntegrationCategory;

import java.util.List;

@Category(IntegrationCategory.class)
public final class TaskEndpointTest extends AbstractIntegrationTest {

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @Before
    public void before() {
        taskEndpoint.createTask(userSession, "Task1", null);
        taskEndpoint.createTask(userSession, "Task2", null);
        taskEndpoint.createTask(adminSession, "AdminTask1", null);
    }

    @After
    public void after() {
        taskEndpoint.clearAllTask(adminSession);
    }

    @Test
    public void testCreateTask() throws Exception {
        @NotNull final String newTaskName = "New task";
        @NotNull final String newTaskDescription = "New task";
        @NotNull final Result result = taskEndpoint.createTask(userSession, newTaskName, newTaskDescription);
        Assert.assertNotNull(result);
        Assert.assertTrue(result.success);
        @Nullable final TaskDto task = taskEndpoint.getTaskByName(userSession, newTaskName);
        Assert.assertNotNull(task);
        Assert.assertEquals(newTaskName, task.getName());
        Assert.assertEquals(newTaskDescription, task.getDescription());
    }

    @Test
    public void testClearTaskList() throws Exception {
        taskEndpoint.clearTaskList(userSession);
        @NotNull final List<TaskDto> tasks = taskEndpoint.getTaskList(userSession);
        Assert.assertEquals(0, tasks.size());
    }

    @Test
    public void testGetTaskList() throws Exception {
        @NotNull final List<TaskDto> userTasks = taskEndpoint.getTaskList(userSession);
        Assert.assertEquals(2, userTasks.size());
        @NotNull final List<TaskDto> adminTasks = taskEndpoint.getTaskList(adminSession);
        Assert.assertEquals(1, adminTasks.size());
    }

    @Test
    public void testRemoveTaskById() throws Exception {
        @NotNull final TaskDto task = taskEndpoint.getTaskByName(userSession, "Task1");
        taskEndpoint.removeTaskById(userSession, task.id);
        @NotNull final List<TaskDto> userTasks = taskEndpoint.getTaskList(userSession);
        Assert.assertEquals(1, userTasks.size());
    }

    @Test
    public void testRemoveTaskByIndex() throws Exception {
        taskEndpoint.removeTaskByIndex(userSession, 0);
        @NotNull final List<TaskDto> userTasks = taskEndpoint.getTaskList(userSession);
        Assert.assertEquals(1, userTasks.size());
    }

    @Test
    public void testRemoveTaskByName() throws Exception {
        taskEndpoint.removeTaskByName(userSession, "Task1");
        @NotNull final List<TaskDto> userTasks = taskEndpoint.getTaskList(userSession);
        Assert.assertEquals(1, userTasks.size());
    }

    @Test
    public void testGetTaskById() throws Exception {
        @NotNull final String id = taskEndpoint.getTaskByName(userSession, "Task1").id;
        @NotNull final TaskDto task = taskEndpoint.getTaskById(userSession, id);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.name, "Task1");
    }

    @Test
    public void testGetTaskByIndex() throws Exception {
        @NotNull final TaskDto task = taskEndpoint.getTaskByIndex(userSession, 0);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.name, "Task1");
    }

    @Test
    public void testGetTaskByName() throws Exception {
        @NotNull final TaskDto task = taskEndpoint.getTaskByName(userSession, "Task1");
        Assert.assertNotNull(task);
        Assert.assertEquals(task.name, "Task1");
    }

    @Test
    public void testUpdateTaskById() throws AbstractException_Exception {
        @NotNull final String newName = "New name";
        @NotNull final String newDescription = "New description";
        @NotNull final String id = taskEndpoint.getTaskByName(userSession, "Task1").id;
        @NotNull final Result result = taskEndpoint.updateTaskById(
                userSession,
                id,
                newName,
                newDescription
        );
        Assert.assertTrue(result.success);
        @NotNull final TaskDto updatedTaskInSystem = taskEndpoint.getTaskById(userSession, id);
        Assert.assertEquals(updatedTaskInSystem.name, newName);
        Assert.assertEquals(updatedTaskInSystem.description, newDescription);
    }

    @Test
    public void testUpdateTaskByIndex() throws AbstractException_Exception {
        @NotNull final String newName = "New name";
        @NotNull final String newDescription = "New description";
        @NotNull final Result result = taskEndpoint.updateTaskByIndex(
                userSession,
                0,
                newName,
                newDescription
        );
        Assert.assertTrue(result.success);
        @NotNull final TaskDto updatedTaskInSystem = taskEndpoint.getTaskByIndex(userSession, 0);
        Assert.assertEquals(updatedTaskInSystem.name, newName);
        Assert.assertEquals(updatedTaskInSystem.description, newDescription);
    }

    @Test
    public void testClearAllTasks() throws AbstractException_Exception {
        @NotNull final Result result = taskEndpoint.clearAllTask(adminSession);
        Assert.assertTrue(result.success);
        @NotNull final List<TaskDto> userTasks = taskEndpoint.getTaskList(userSession);
        Assert.assertEquals(0, userTasks.size());
        @NotNull final List<TaskDto> adminTasks = taskEndpoint.getTaskList(adminSession);
        Assert.assertEquals(0, userTasks.size());
    }

    @Test
    public void testGetAllTaskList() throws AbstractException_Exception {
        @NotNull final List<TaskDto> tasks = taskEndpoint.getAllTaskList(adminSession);
        Assert.assertEquals(3, tasks.size());
    }

}
# DefaultApi

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**countProjects**](DefaultApi.md#countProjects) | **GET** /api/v1/projects/count | 
[**countTasks**](DefaultApi.md#countTasks) | **GET** /api/v1/tasks/count | 
[**createProject**](DefaultApi.md#createProject) | **POST** /api/v1/project | 
[**createProjects**](DefaultApi.md#createProjects) | **POST** /api/v1/projects | 
[**createTask**](DefaultApi.md#createTask) | **POST** /api/v1/task | 
[**createTasks**](DefaultApi.md#createTasks) | **POST** /api/v1/tasks | 
[**deleteAllProjects**](DefaultApi.md#deleteAllProjects) | **DELETE** /api/v1/projects/all | 
[**deleteAllTasks**](DefaultApi.md#deleteAllTasks) | **DELETE** /api/v1/tasks/all | 
[**deleteProjectById**](DefaultApi.md#deleteProjectById) | **DELETE** /api/v1/project/{id} | 
[**deleteProjects**](DefaultApi.md#deleteProjects) | **DELETE** /api/v1/projects | 
[**deleteTaskById**](DefaultApi.md#deleteTaskById) | **DELETE** /api/v1/task/{id} | 
[**deleteTasks**](DefaultApi.md#deleteTasks) | **DELETE** /api/v1/tasks | 
[**existProjectById**](DefaultApi.md#existProjectById) | **GET** /api/v1/project/exists/{id} | 
[**existTaskById**](DefaultApi.md#existTaskById) | **GET** /api/v1/task/exists/{id} | 
[**findAllProjects**](DefaultApi.md#findAllProjects) | **GET** /api/v1/projects | 
[**findAllTasks**](DefaultApi.md#findAllTasks) | **GET** /api/v1/tasks | 
[**findProjectById**](DefaultApi.md#findProjectById) | **GET** /api/v1/project/{id} | 
[**findTaskById**](DefaultApi.md#findTaskById) | **GET** /api/v1/task/{id} | 
[**updateProjectPatch**](DefaultApi.md#updateProjectPatch) | **PATCH** /api/v1/project | 
[**updateProjectPut**](DefaultApi.md#updateProjectPut) | **PUT** /api/v1/project | 
[**updateProjectsPatch**](DefaultApi.md#updateProjectsPatch) | **PATCH** /api/v1/projects | 
[**updateProjectsPut**](DefaultApi.md#updateProjectsPut) | **PUT** /api/v1/projects | 
[**updateTaskPatch**](DefaultApi.md#updateTaskPatch) | **PATCH** /api/v1/task | 
[**updateTaskPut**](DefaultApi.md#updateTaskPut) | **PUT** /api/v1/task | 
[**updateTasksPatch**](DefaultApi.md#updateTasksPatch) | **PATCH** /api/v1/tasks | 
[**updateTasksPut**](DefaultApi.md#updateTasksPut) | **PUT** /api/v1/tasks | 


<a name="countProjects"></a>
# **countProjects**
> Long countProjects()



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    Long result = apiInstance.countProjects();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#countProjects");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**Long**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="countTasks"></a>
# **countTasks**
> Long countTasks()



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    Long result = apiInstance.countTasks();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#countTasks");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**Long**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="createProject"></a>
# **createProject**
> createProject(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
ProjectDto body = new ProjectDto(); // ProjectDto | 
try {
    apiInstance.createProject(body);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectDto**](ProjectDto.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="createProjects"></a>
# **createProjects**
> createProjects(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
List<ProjectDto> body = Arrays.asList(new ProjectDto()); // List<ProjectDto> | 
try {
    apiInstance.createProjects(body);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createProjects");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**List&lt;ProjectDto&gt;**](ProjectDto.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="createTask"></a>
# **createTask**
> createTask(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
TaskDto body = new TaskDto(); // TaskDto | 
try {
    apiInstance.createTask(body);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createTask");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TaskDto**](TaskDto.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="createTasks"></a>
# **createTasks**
> createTasks(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
List<TaskDto> body = Arrays.asList(new TaskDto()); // List<TaskDto> | 
try {
    apiInstance.createTasks(body);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createTasks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**List&lt;TaskDto&gt;**](TaskDto.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteAllProjects"></a>
# **deleteAllProjects**
> deleteAllProjects()



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    apiInstance.deleteAllProjects();
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteAllProjects");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteAllTasks"></a>
# **deleteAllTasks**
> deleteAllTasks()



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    apiInstance.deleteAllTasks();
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteAllTasks");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteProjectById"></a>
# **deleteProjectById**
> deleteProjectById(id)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    apiInstance.deleteProjectById(id);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteProjectById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteProjects"></a>
# **deleteProjects**
> deleteProjects(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
List<ProjectDto> body = Arrays.asList(new ProjectDto()); // List<ProjectDto> | 
try {
    apiInstance.deleteProjects(body);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteProjects");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**List&lt;ProjectDto&gt;**](ProjectDto.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteTaskById"></a>
# **deleteTaskById**
> deleteTaskById(id)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    apiInstance.deleteTaskById(id);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteTaskById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteTasks"></a>
# **deleteTasks**
> deleteTasks(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
List<TaskDto> body = Arrays.asList(new TaskDto()); // List<TaskDto> | 
try {
    apiInstance.deleteTasks(body);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteTasks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**List&lt;TaskDto&gt;**](TaskDto.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="existProjectById"></a>
# **existProjectById**
> Boolean existProjectById(id)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    Boolean result = apiInstance.existProjectById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#existProjectById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

**Boolean**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="existTaskById"></a>
# **existTaskById**
> Boolean existTaskById(id)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    Boolean result = apiInstance.existTaskById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#existTaskById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

**Boolean**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="findAllProjects"></a>
# **findAllProjects**
> List&lt;ProjectDto&gt; findAllProjects()



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    List<ProjectDto> result = apiInstance.findAllProjects();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#findAllProjects");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;ProjectDto&gt;**](ProjectDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="findAllTasks"></a>
# **findAllTasks**
> List&lt;TaskDto&gt; findAllTasks()



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    List<TaskDto> result = apiInstance.findAllTasks();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#findAllTasks");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;TaskDto&gt;**](TaskDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="findProjectById"></a>
# **findProjectById**
> ProjectDto findProjectById(id)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    ProjectDto result = apiInstance.findProjectById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#findProjectById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

[**ProjectDto**](ProjectDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="findTaskById"></a>
# **findTaskById**
> TaskDto findTaskById(id)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    TaskDto result = apiInstance.findTaskById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#findTaskById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

[**TaskDto**](TaskDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateProjectPatch"></a>
# **updateProjectPatch**
> updateProjectPatch(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
ProjectDto body = new ProjectDto(); // ProjectDto | 
try {
    apiInstance.updateProjectPatch(body);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updateProjectPatch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectDto**](ProjectDto.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="updateProjectPut"></a>
# **updateProjectPut**
> updateProjectPut(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
ProjectDto body = new ProjectDto(); // ProjectDto | 
try {
    apiInstance.updateProjectPut(body);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updateProjectPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectDto**](ProjectDto.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="updateProjectsPatch"></a>
# **updateProjectsPatch**
> updateProjectsPatch(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
List<ProjectDto> body = Arrays.asList(new ProjectDto()); // List<ProjectDto> | 
try {
    apiInstance.updateProjectsPatch(body);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updateProjectsPatch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**List&lt;ProjectDto&gt;**](ProjectDto.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="updateProjectsPut"></a>
# **updateProjectsPut**
> updateProjectsPut(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
List<ProjectDto> body = Arrays.asList(new ProjectDto()); // List<ProjectDto> | 
try {
    apiInstance.updateProjectsPut(body);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updateProjectsPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**List&lt;ProjectDto&gt;**](ProjectDto.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="updateTaskPatch"></a>
# **updateTaskPatch**
> updateTaskPatch(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
TaskDto body = new TaskDto(); // TaskDto | 
try {
    apiInstance.updateTaskPatch(body);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updateTaskPatch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TaskDto**](TaskDto.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="updateTaskPut"></a>
# **updateTaskPut**
> updateTaskPut(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
TaskDto body = new TaskDto(); // TaskDto | 
try {
    apiInstance.updateTaskPut(body);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updateTaskPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TaskDto**](TaskDto.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="updateTasksPatch"></a>
# **updateTasksPatch**
> updateTasksPatch(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
List<TaskDto> body = Arrays.asList(new TaskDto()); // List<TaskDto> | 
try {
    apiInstance.updateTasksPatch(body);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updateTasksPatch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**List&lt;TaskDto&gt;**](TaskDto.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="updateTasksPut"></a>
# **updateTasksPut**
> updateTasksPut(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
List<TaskDto> body = Arrays.asList(new TaskDto()); // List<TaskDto> | 
try {
    apiInstance.updateTasksPut(body);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updateTasksPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**List&lt;TaskDto&gt;**](TaskDto.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

